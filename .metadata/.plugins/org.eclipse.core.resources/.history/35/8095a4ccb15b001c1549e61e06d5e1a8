#include "ili9341.h"
#include "main.h"

//ILI9341 Datasheet: https://cdn-shop.adafruit.com/datasheets/ILI9341.pdf

static SPI_HandleTypeDef LCD_SPIhandle;

void LCD_Init(SPI_HandleTypeDef *SPI_Handle) {

	memcpy(&LCD_SPIhandle, SPI_Handle, sizeof(*SPI_Handle));

	HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_SET); //chip select always high unless writing data
	HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_Pin, GPIO_PIN_SET); //Software reset

	// Register values gotten from Mohamed Yaqoob's ili9341 library:

	LCD_WriteReg(LCD_RESET); //Hardware reset
	HAL_Delay(100);
	LCD_WriteReg(LCD_DISPLAY_OFF); //Turn off display
	LCD_WriteReg(LCD_POWER1); //Set GVDD level to 4.75V
	LCD_WriteData(0x26);
	LCD_WriteReg(LCD_POWER2); //Sets factor used in step up circuit for optimal operating voltage
	LCD_WriteData(0x11);
	LCD_WriteReg(LCD_VCOM1); //Sets VCOMH voltage: 4.025V and VCOML voltage: -0.925V
	LCD_WriteData(0x35);
	LCD_WriteData(0x3e);
	LCD_WriteReg(LCD_VCOM2); //Sets offset voltage: VMH/VML + 31
	LCD_WriteData(0xBE);
	LCD_WriteReg(LCD_MAC); //Should be removed.
	LCD_WriteData(0x48);
	LCD_WriteReg(LCD_PIXEL_FORMAT);
	LCD_WriteData(0x55);
	LCD_WriteReg(LCD_FRC);
	LCD_WriteData(0);
	LCD_WriteData(0x1F);
	LCD_WriteReg(LCD_COLUMN_ADDR);
	LCD_WriteData(0x00);
	LCD_WriteData(0x00);
	LCD_WriteData(0x00);
	LCD_WriteData(0xEF);
	LCD_WriteReg(LCD_PAGE_ADDR);
	LCD_WriteData(0x00);
	LCD_WriteData(0x00);
	LCD_WriteData(0x01);
	LCD_WriteData(0x3F);
	LCD_WriteReg(LCD_TEARING_OFF);
	LCD_WriteReg(LCD_Entry_Mode_Set);
	LCD_WriteData(0x07);
	LCD_WriteReg(LCD_DFC);
	LCD_WriteData(0x0A);
	LCD_WriteData(0x82);
	LCD_WriteData(0x27);
	LCD_WriteData(0x00);
	LCD_WriteReg (LCD_SLEEP_OUT);
	HAL_Delay(100);
	LCD_WriteReg (LCD_DISPLAY_ON); //Turn on display
	HAL_Delay(100);
	LCD_WriteReg (LCD_GRAM);
	HAL_Delay(5);
	LCD_WriteReg(LCD_MAC);
	LCD_WriteData(LCD_MADCTL_MV | LCD_MADCTL_BGR | LCD_MAD_MX | LCD_MAD_MY); //Landscape and mirror image


}

void LCD_WriteReg(uint8_t reg) {

	HAL_GPIO_WritePin(LCD_DC_GPIO_Port, LCD_DC_Pin, GPIO_PIN_RESET); //Set data control to low for register writing
	HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_RESET); //Set chip select low when transmitting
	HAL_SPI_Transmit(&LCD_SPIhandle, &reg, 1, 5); //Send register
	HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_SET); //Set chip select high after transmit
}


void LCD_WriteData(uint8_t data) {

	HAL_GPIO_WritePin(LCD_DC_GPIO_Port, LCD_DC_Pin, GPIO_PIN_SET); //Set data control to high for writing data
	HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_RESET); //Set chip select low when transmitting
	HAL_SPI_Transmit(&LCD_SPIhandle, &data, 1, 5); //Send data
	HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_SET); //Set chip select high after transmit
}

void LCD_SetScreenSize() { //Sets "cursor" position to full screen

	LCD_WriteReg(LCD_COLUMN_ADDR); //Sets vertical distance of 0-320
	LCD_WriteData(LCD_STARTX >> 8);
	LCD_WriteData(LCD_STARTX & 0xFF);
	LCD_WriteData( (LCD_HEIGHT - 1) >> 8);
	LCD_WriteData( (LCD_HEIGHT - 1) & 0xFF);

	LCD_WriteReg(LCD_PAGE_ADDR); //Sets horizontal distance of 0-240
	LCD_WriteData(LCD_STARTY >> 8);
	LCD_WriteData(LCD_STARTY & 0xFF);
	LCD_WriteData( (LCD_WIDTH - 1) >> 8);
	LCD_WriteData( (LCD_WIDTH - 1) & 0xFF);
  	LCD_WriteReg(LCD_GRAM);
}

void LCD_ClearScreen() {

	LCD_SetScreenSize();

	for (uint32_t i = 0; i < (LCD_WIDTH * LCD_HEIGHT); i++) {
		LCD_WriteData(0x0000 >> 8);
		LCD_WriteData(0x0000 & 0xFF);
	}

}

void LCD_DisplayImage(const uint16_t *data) {

	LCD_SetScreenSize();

	for(uint32_t i = 0; i < (LCD_WIDTH * LCD_HEIGHT); i++) {
		LCD_WriteData(data[i] >> 8);
		LCD_WriteData(data[i] & 0xFF);
	}
}

void LCD_RedScreen() { //used for debugging

	LCD_SetScreenSize();

	for (uint32_t i = 0; i < (LCD_WIDTH * LCD_HEIGHT); i++) {
		LCD_WriteData(0xF800 >> 8);
		LCD_WriteData(0xF800 & 0xFF);
	}

}
