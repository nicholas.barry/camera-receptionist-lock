#include "ov7670.h"
#include "main.h"

//The documentation was very bad for the ov7670. Therefore most of the information
//found on the camera came from third party sources like Petr Machala

//OV7670 Datasheet: http://web.mit.edu/6.111/www/f2016/tools/OV7670_2006.pdf

const uint8_t OV7670_reg[][2] = {

  //Color related
  {0x12, 0x14},   // Output format of QVGA (240x320) RGB (Red Green Blue)
  {0x8C, 0x00},   // Disable RGB444 - required to enable RGB565
  {0x40, 0xD0},   // Enable RGB565
  {0x3A, 0x0C},   // UYVY (16-bit color format)
  {0x3D, 0x80},   // gamma enable, UV auto adjust, UYVY
  {0xB0, 0x84},   // Reserved that needs to be set to make it work? (color mode)

  //Clock related - makes image less blurry
  {0x0C, 0x04},   // DCW enable - has to do with clock scaling
  {0x3E, 0x19},   // Enable manual scaling and divide PCLK by 2
  {0x70, 0x3A},   // scaling_xsc
  {0x71, 0x35},   // scaling_ysc
  {0x72, 0x11},   // verticle/horizontal down sample by 2
  {0x73, 0xf1},   // Enable DSP clock and divide by 2

  //Size of image (Found based on limited information provided in data sheet, looking at examples, and guess and check)
  {0x17, 0x16},   // HSTART
  {0x18, 0x04},   // HSTOP
  {0x32, 0x80},   // HREF
  {0x19, 0x03},   // VSTART
  {0x1A, 0x7B},   // VSTOP
  {0x03, 0x0A},   // VREF

  {0x41, 0x38},   // Edge enhancement, de-noise and enable AWB gain

};

static DCMI_HandleTypeDef **CAM_DCMIhandle;
static I2C_HandleTypeDef **CAM_I2Chandle;
extern uint16_t frame_buffer[320*240]; //Extends visibility of frame buffer so it can be seen within this file

void CAM_Init(DCMI_HandleTypeDef *DCMI_Handle, I2C_HandleTypeDef *I2C_Handle) { //Initialize camera

	//memcpy(&CAM_DCMIhandle, DCMI_Handle, sizeof(*DCMI_Handle)); //Copy handlers from memory
	//memcpy(&CAM_I2Chandle, I2C_Handle, sizeof(*I2C_Handle));

	CAM_DCMIhandle = &DCMI_Handle;
	CAM_I2Chandle = &I2C_Handle;

	  HAL_GPIO_WritePin(CAM_RST_GPIO_Port, CAM_RST_Pin, GPIO_PIN_RESET); //Hardware reset for camera
	  HAL_Delay(100);
	  HAL_GPIO_WritePin(CAM_RST_GPIO_Port, CAM_RST_Pin, GPIO_PIN_SET);
	  HAL_Delay(100);

	  HAL_Delay(30);

	  CAM_StopCapture(); //Stop capture if one is occurring
	  CAM_Write(0x12, 0x80);  // Reset initialization to default
	  HAL_Delay(30);
	  for(int i = 0; i < OV7670_REG_NUM; i++) { //Write registers to camera
		CAM_Write(OV7670_reg[i][0], OV7670_reg[i][1]);
	    HAL_Delay(1);
	  }

}

void CAM_Capture() { //Capture image

	for (uint8_t i = 0; i < 2; i++) { //Capture twice to fill entire screen since dma can only handle around 65KB

		CAM_StopCapture(); //Stop ongoing capture
		if (HAL_DCMI_Start_DMA(&(**CAM_DCMIhandle), DCMI_MODE_SNAPSHOT, (uint32_t)frame_buffer, (LCD_WIDTH * LCD_HEIGHT) / 2) != HAL_OK) { //Capture image in snapshot mode and place in framebuffer
			while(1) {
				LCD_RedScreen(); //Display red screen if capture fails
				HAL_Delay(100000);
			}
		}
	}
}

void CAM_StopCapture() { //Stops ongoing capture
	HAL_DCMI_Stop(&(**CAM_DCMIhandle));
}

void CAM_Write(uint8_t regAddr, uint8_t data) {

	if (HAL_I2C_Mem_Write(&(**CAM_I2Chandle), (uint16_t)CAM_ADDR, regAddr, I2C_MEMADD_SIZE_8BIT, &data, 1, HAL_MAX_DELAY) != HAL_OK) { //Write given register and data to camera
		while(1) {
			LCD_RedScreen(); //Display red screen if write fails
			HAL_Delay(100000);
		}
	}

}

void Delay(uint32_t seconds) { //Delay for 1 sec * input
	for (uint8_t i = 0; i < seconds; i ++) {
		HAL_Delay(1000);
	}
}

