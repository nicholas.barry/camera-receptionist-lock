#include "ov7670.h"
#include "main.h"

const uint8_t OV7670_reg[][2] = {
  // Color mode related
  {0x12, 0x14},   // QVGA, RGB
  {0x8C, 0x00},   // RGB444 Disable
  {0x40, 0xd0},   // RGB565, 00 - FF
  {0x3A, 0x0C},   // UYVY
  {0x3D, 0x80},   // gamma enable, UV auto adjust, UYVY
  {0xB0, 0x84},


  // clock related
  {0x0C, 0x04},  // DCW enable
  {0x3E, 0x19},  // manual scaling, pclk/=2
  {0x70, 0x3A},  // scaling_xsc
  {0x71, 0x35},  // scaling_ysc
  {0x72, 0x11}, // down sample by 2
  {0x73, 0xf1}, // DSP clock /= 2


  // windowing
  {0x17, 0x16},   // HSTART
  {0x18, 0x04},   // HSTOP
  {0x32, 0x80},   // HREF
  {0x19, 0x03},   // VSTART
  {0x1a, 0x7b},   // VSTOP
  {0x03, 0x0a},   // VREF

  // color matrix coefficient
  {0x4f, 0x80},
  {0x50, 0x80},
  {0x51, 0x00},
  {0x52, 0x22},
  {0x53, 0x5e},
  {0x54, 0x80},
  {0x58, 0x9e},

  {0x41, 0x38},   // edge enhancement, de-noise, AWG gain enabled

  // gamma curve
  {0x7b, 0x10},
  {0x7c, 0x1E},
  {0x7d, 0x35},
  {0x7e, 0x5A},
  {0x7f, 0x69},
  {0x80, 0x76},
  {0x81, 0x82},
  {0x82, 0x8C},
  {0x83, 0x96},
  {0x84, 0xA0},
  {0x85, 0xB4},
  {0x86, 0xC3},
  {0x87, 0xD7},
  {0x88, 0xE6},
  {0x89, 0xF4},
  {0x7a, 0x10},

  {0x11, 0x00}, // pre-scalar = 1/1

  //{0x13, 0xE7},
  //{0x13, 0xE5},
  //{0x01, 0x84},
  //{0x02, 0x4C},
  //{0x6F, 0x9F},   // Simple AWB


};

static DCMI_HandleTypeDef CAM_DCMIhandle;
static I2C_HandleTypeDef CAM_I2Chandle;
extern uint16_t frame_buffer[320*240];

void CAM_Init(DCMI_HandleTypeDef *DCMI_Handle, I2C_HandleTypeDef *I2C_Handle) { //Initialize camera

	memcpy(&CAM_DCMIhandle, DCMI_Handle, sizeof(*DCMI_Handle)); //Copy handlers from memory
	memcpy(&CAM_I2Chandle, I2C_Handle, sizeof(*I2C_Handle));

	  HAL_GPIO_WritePin(CAM_RST_GPIO_Port, CAM_RST_Pin, GPIO_PIN_RESET); //Hardware reset for camera
	  HAL_Delay(100);
	  HAL_GPIO_WritePin(CAM_RST_GPIO_Port, CAM_RST_Pin, GPIO_PIN_SET);
	  HAL_Delay(100);

	  //DMA init was here

	  HAL_Delay(30);

	  CAM_StopCapture(); //Stop capture if one is occurring
	  CAM_Write(0x12, 0x80);  // Reset initialization to default
	  HAL_Delay(30);
	  for(int i = 0; i < OV7670_REG_NUM; i++) { //Write registers to camera
		CAM_Write(OV7670_reg[i][0], OV7670_reg[i][1]);
	    HAL_Delay(1);
	  }

}

void CAM_Capture() { //Capture image

	for (uint8_t i = 0; i < 2; i++) { //Capture twice to fill entire screen since dma can only handle around 65KB

		CAM_StopCapture(); //Stop ongoing capture
		if (HAL_DCMI_Start_DMA(&CAM_DCMIhandle, DCMI_MODE_SNAPSHOT, (uint32_t)frame_buffer, (LCD_WIDTH * LCD_HEIGHT) / 2) != HAL_OK) { //Capture image in snapshot mode and place in framebuffer
			while(1) {
				LCD_RedScreen(); //Display red screen if capture fails
				HAL_Delay(100000);
			}
		}
	}
}

void CAM_StopCapture() { //Stops ongoing capture
	HAL_DCMI_Stop(&CAM_DCMIhandle);
}

void CAM_Write(uint8_t regAddr, uint8_t data) {

	if (HAL_I2C_Mem_Write(&CAM_I2Chandle, (uint16_t)CAM_ADDR, regAddr, I2C_MEMADD_SIZE_8BIT, &data, 1, HAL_MAX_DELAY) != HAL_OK) { //Write given register and data to camera
		while(1) {
			LCD_RedScreen(); //Display red screen if write fails
			HAL_Delay(100000);
		}
	}

}

void Delay(uint32_t seconds) { //Delay for 1 sec * input
	for (uint8_t i = 0; i < seconds; i ++) {
		HAL_Delay(1000);
	}
}

