#include "stm32f4xx_hal.h"

#define LCD_WIDTH       				240  // Width of LCD
#define LCD_HEIGHT      				320  // Height of LCD
#define LCD_STARTX						0	 // Starting point of screen in X direction
#define LCD_STARTY						0	 // Starting point of screen in Y direction
#define LCD_RESET			 		    0x01 // Software reset - resets the commands and parameters to their S/W Reset default values.
#define LCD_SLEEP_OUT		  			0x11 // Avoids abnormal visual effects
#define LCD_DISPLAY_OFF					0x28 // Output from frame memory is disabled and black page is inserted
#define LCD_DISPLAY_ON					0x29 // Used to recover from display off mode - enables output from frame memory
#define LCD_COLUMN_ADDR					0x2A // Used to define area of frame memory where MCU can access (horizontal)
#define LCD_PAGE_ADDR			  		0x2B // Used to define area of frame memory where MCU can access (vertical)
#define LCD_GRAM				    	0x2C // Graphic RAM to store display data
#define LCD_TEARING_OFF					0x34 // Turn OFF (Active Low) the Tearing Effect output signal from the TE signal line.

/*NOTE: Screen tearing is a visual artifact in video display where a display device shows information from multiple frames in a
 * single screen draw. The artifact occurs when the video feed to the device is not in sync with the display's refresh rate.
 */

#define LCD_MAC			        		0x36 // Mem. Access Ctrl - Defines read/write scanning direction of frame memory
#define LCD_PIXEL_FORMAT    			0x3A // Sets the pixel format for the RGB image data used by the interface. DPI [2:0]
											 // is the pixel format select of RGB interface and DBI [2:0] is the pixel format of MCU interface
#define LCD_FRC					    	0xB1 // Frame Rate Ctrl - Sets the division ratio for internal clocks of Normal mode at MCU interface.
#define LCD_DFC				 	    	0xB6 // Display Function Ctrl - Allows you to change if the LCD is normally black/white, set scan mode, etc.
#define LCD_Entry_Mode_Set				0xB7 // Set low voltage detection control / output level of gate driver
#define LCD_POWER1						0xC0 // Set the GVDD level, which is a reference level for the VCOM level and the grayscale voltage level.
#define LCD_POWER2						0xC1 // Sets the factor used in the step-up circuits.
#define LCD_VCOM1						0xC5 // Set the VCOMH/VCOML voltage
#define LCD_VCOM2						0xC7 // Set the VCOM offset voltage.
#define LCD_MADCTL_MV  					0x20 // Row/Column exchange
#define LCD_MADCTL_BGR 					0x08 // RGB -> BGR
#define LCD_MAD_MY						0x80 // Mirror rows
#define LCD_MAD_MX						0x40 // Mirror columns

void LCD_Init(SPI_HandleTypeDef *SPI_Handle); //Initialize LCD
void LCD_WriteReg(uint8_t reg); //Write register to LCD via SPI
void LCD_WriteData(uint8_t data); //Write data to LCD via SPI
void LCD_SetScreenSize(); //Set cursor position to top left and bottom right of screen
void LCD_ClearScreen(); //Makes screen black
void LCD_DisplayImage(const uint16_t *data); //Print image to screen from buffer of size 240*320
void LCD_RedScreen(); //Makes screen red (used for error checking)
extern void memcopy(void *dest, void *source, size_t size); //Extends visibility of function so it can be seen within the source file

