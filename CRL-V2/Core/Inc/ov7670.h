#include "ili9341.h"

#define CAM_ADDR 		0x42 //i2c address for camera
#define OV7670_REG_NUM 	19   //number of registers initialized for camera

void CAM_Init(DCMI_HandleTypeDef *DCMI_Handle, I2C_HandleTypeDef *I2C_Handle); //Initialize camera
void CAM_Capture(); //Capture image
void CAM_StopCapture(); //Stop camera capture
void CAM_Write(uint8_t regAddr, uint8_t data); //write data to camera via i2c
void Delay(uint32_t seconds); //1 second delay
extern void memcopy(void *dest, void *source, size_t size); //Extends visibility of function so it can be seen within the source file
